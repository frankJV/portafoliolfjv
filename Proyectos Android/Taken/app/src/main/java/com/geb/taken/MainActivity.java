package com.geb.taken;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String nombre = "";
    private Button bAceptar;
    private EditText ETNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();


        bAceptar = findViewById(R.id.BAceptar);
        ETNombre = findViewById(R.id.nombre1);
    }

    public void Aceptar(View view){
        nombre = ETNombre.getText().toString();

        if(!nombre.isEmpty()){
            Intent intent = new Intent(MainActivity.this,Principal.class);
            intent.putExtra("Nombre",nombre);
            startActivity(intent);
        }

        else{
            Toast.makeText(this,"Favor de ingresar un nombre",Toast.LENGTH_SHORT).show();
        }

    }
}