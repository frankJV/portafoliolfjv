package com.geb.taken;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Tablero extends AppCompatActivity {

    private String nombre;
    private TextView BBienvenida;
    private int posicionX = 3;
    private int posicionY = 3;
    private RelativeLayout tablero;
    private Button[][] botones;
    private int[] piezas;
    private TextView TVMovimientos, TVCountTiempo;
    private int numMov;
    private Timer timer;
    private int ContadorTiempo = 0;
    private Button BPausar;
    private Button BRevolver;
    private boolean TiempoCorriendo;
    private MiBase miBase;
    private String dispGanadora = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablero);
        getSupportActionBar().hide();

        Bundle bundle = getIntent().getExtras();
        nombre = bundle.getString("Nombre");

        BBienvenida = findViewById(R.id.Bienvenida1);
        BBienvenida.setText("Jugador: " + nombre);

        CargarVistas();
        CargarNumeros();
        GenerarNumeros();
        CargarDatosAVistas();
    }

    //Se colocan los numeros a cada uno de los botones o piezas
    private void CargarDatosAVistas(){
        posicionX = 3;
        posicionY = 3;

        for(int i=0; i<tablero.getChildCount()-1; i++){//getChildrenCount() obtiene la cantidad de hijos que contiene el LinearLayout, en este cas, la cantidad de botones
            botones[i/4][i%4].setText(String.valueOf(piezas[i]));
            botones[i/4][i%4].setBackgroundResource(android.R.drawable.btn_default);
        }


        //int[] ordenGanador = {15,14,13,12,11,10,9,8,7,6,5,4,3,2,1}; // Pruebas imposible
        //int[] ordenGanador = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15}; // Pruebas Default
        //int[] ordenGanador = {1,2,3,4,12,13,14,5,11,15,6,7,10,9,8}; // Pruebas Periferico
        //int[] ordenGanador = {7,8,9,10,6,1,2,11,5,4,3,12,15,14,13}; // Pruebas Espiral
        //int[] ordenGanador = {1,5,9,13,2,6,10,14,3,7,11,15,4,8,12}; //Pruebas vertical
        /*for(int i=0; i<tablero.getChildCount()-1; i++) {
            botones[i/4][i%4].setText(String.valueOf(ordenGanador[i]));
            botones[i/4][i%4].setBackgroundResource(android.R.drawable.btn_default);
        }*/
        botones[posicionX][posicionY].setText("");
        botones[posicionX][posicionY].setBackgroundColor(ContextCompat.getColor(this,R.color.colorFreeButton));
    }

    //Funcion que genera numeros de forma aleatoria en un rango entre 1 y 15
    //Dicho numero generado será la posición donde se almacene n, el cual va en decremento inioiando en 15
    private void GenerarNumeros(){
        int n = 15;
        Random random = new Random();
        while(n>1){
            int randomNum = random.nextInt(n--);//Se genera un numero aleatorio entre 0 y n
            int temp = piezas[randomNum];
            piezas[randomNum] = piezas[n];
            piezas[n] = temp;
        }

        if(!TieneSolucion())
             GenerarNumeros();;
    }

    //Indica qué tan lejos (o cerca) está la matriz de ser ordenada. Si la matriz ya está ordenada, entonces el recuento de inversión es 0.
    private boolean TieneSolucion(){
        int contarInversiones = 0;
        for(int i=0; i<15; i++){
            for(int j=0; j<i; j++){
                if(piezas[j] > piezas[i])
                    contarInversiones++;
            }
        }
        return contarInversiones%2 == 0;
    }

    //Se crea un arreglo de enteros, en el cual se almacenaran los valores, dependiendo de la cantidad de hijos (botones) del Linear Layout, pero omitiendo 1, que corresponde al boton invisible.
    private void CargarNumeros(){
        piezas = new int[16];
        for(int i=0; i<tablero.getChildCount()-1; i++){
            piezas[i] = i + 1;
        }
    }

    //Se obtiene la vista o ubicación de cada boton dentro del LinearLayour.
    private void CargarVistas(){
        tablero = findViewById(R.id.grupo);
        TVMovimientos = findViewById(R.id.numMovimientos);
        TVCountTiempo = findViewById(R.id.tiempo);
        botones = new Button[4][4];
        BRevolver = findViewById(R.id.BRevolver);
        BPausar = findViewById(R.id.BPausar);

        IniciarTemporizador();

        for(int i=0; i<tablero.getChildCount(); i++){
            botones[i/4][i%4] = (Button) tablero.getChildAt(i);//Se determinan la posición de cada boton y se obtiene su vista
        }
    }

    //  Se programa el temporizador para que inicie su ejecución
    private void IniciarTemporizador(){
        TiempoCorriendo = true;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                ContadorTiempo++;
                fijarTiempo(ContadorTiempo);
            }
        },1000, 1000);
    }

    //Se realiza la conversión del contador y se coloca en su respectivo TextView
    private void fijarTiempo(int ContadorTiempo){
        int segundo = ContadorTiempo % 60;
        int hora = ContadorTiempo /3600;
        int minuto = (ContadorTiempo - hora * 3600) / 60;

        TVCountTiempo.setText(String.format("Tiempo: %02d:%02d:%02d", hora, minuto, segundo));
    }


    //Se realizan los movimientos. En este caso, se intercambian los atributos y texto del boton vacio y el boton periferico que se desea mover.
    public void ClickBoton(View view){
        Button boton = (Button) view;
        int x = boton.getTag().toString().charAt(0) - '0';
        int y = boton.getTag().toString().charAt(1) - '0';

        if((Math.abs(posicionX-x)==1 && posicionY==y) || (Math.abs(posicionY-y)==1 && posicionX==x)){
            botones[posicionX][posicionY].setText(boton.getText().toString());
            botones[posicionX][posicionY].setBackgroundResource(android.R.drawable.btn_default);
            boton.setText("");
            boton.setBackgroundColor(ContextCompat.getColor(this,R.color.colorFreeButton));
            posicionX = x;
            posicionY = y;
            numMov++;//Se van sumando los movimientos
            TVMovimientos.setText("Movimientos: " + numMov);

            Ganar();
        }
    }

    private void Ganar(){
        boolean HazGanado = false;

        //Disposición por default
        if(posicionX == 3 && posicionY == 3 && botones[0][0].getText().toString().equals("1") && botones[0][1].getText().toString().equals("2")){
            dispGanadora = "Clásica";
            for(int i=0; i<tablero.getChildCount()-1; i++){
                if(botones[i/4][i%4].getText().toString().equals(String.valueOf(i+1))) {//Se comparan los numeros de cada botón con la variable i+1 que va en autoincremento
                    HazGanado = true;
                }
                else{
                    HazGanado = false;
                    break;
                }
            }
        }

        //Disposición imposible
        else if(posicionX == 3 && posicionY == 3  && botones[0][0].getText().toString().equals("15")){
            dispGanadora = "Imposible";
            //Toast.makeText(this,"Haz Ganado",Toast.LENGTH_LONG).show();
            int count = 15;
            for(int i=0; i<tablero.getChildCount()-1; i++){
                if(botones[i/4][i%4].getText().toString().equals(String.valueOf(count))) {//Se comparan los numeros de cada botón con la variable i+1 que va en autoincremento
                    HazGanado = true;
                }
                else{
                    HazGanado = false;
                    //BBienvenida.setText(botones[i/4][i%4].getText().toString());
                    break;
                }
                count--;
            }
        }

        //Disposición vertical
        else if(posicionX == 3 && posicionY == 3  && botones[1][0].getText().toString().equals("2")){
            dispGanadora = "Vertical";
            //Toast.makeText(this,"Haz Ganado",Toast.LENGTH_LONG).show();
            int[] ordenGanador = {1,5,9,13,2,6,10,14,3,7,11,15,4,8,12};
            for(int i=0; i<tablero.getChildCount()-1; i++){
                if(botones[i/4][i%4].getText().toString().equals(String.valueOf(ordenGanador[i]))) {//Se comparan los numeros de cada botón con la variable i+1 que va en autoincremento
                    HazGanado = true;
                }
                else{
                    HazGanado = false;
                    //BBienvenida.setText(botones[i/4][i%4].getText().toString());
                    break;
                }
            }
        }

        //Disposición periferico
        else if(posicionX == 2 && posicionY == 1){
            dispGanadora = "Periférico";
            //Toast.makeText(this,"Haz Ganado",Toast.LENGTH_LONG).show();
            int[] ordenGanador = {1,2,3,4,12,13,14,5,11,0,15,6,10,9,8,7};
            for(int i=0; i<tablero.getChildCount(); i++){
                if(i != 9) {
                    if (botones[i/4][i%4].getText().toString().equals(String.valueOf(ordenGanador[i]))) {//Se comparan los numeros de cada botón con los numeros en el arrego ordenGanador
                        HazGanado = true;
                    }
                    else{
                        HazGanado = false;
                        //BBienvenida.setText(String.valueOf(ordenGanador[i]) + "/" + botones[i%4][i/4].getText().toString());
                        break;
                    }
                }
            }
        }

        //Disposición Espiral
        else if(posicionX == 3 && posicionY == 0){
            dispGanadora = "Espiral";
            //Toast.makeText(this,"Haz Ganado",Toast.LENGTH_LONG).show();
            int[] ordenGanador = {7,8,9,10,6,1,2,11,5,4,3,12,0,15,14,13};
            for(int i=0; i<tablero.getChildCount(); i++){
                if(i != 12) {
                    if (botones[i/4][i%4].getText().toString().equals(String.valueOf(ordenGanador[i]))) {//Se comparan los numeros de cada botón con los numeros en el arrego ordenGanador
                        HazGanado = true;
                    }
                    else{
                        HazGanado = false;
                        //BBienvenida.setText(String.valueOf(ordenGanador[i]) + "/" + botones[i%4][i/4].getText().toString());
                        break;
                    }
                }
            }
        }

        if(HazGanado){
            Toast.makeText(this,"Haz Ganado \nMovimientos: " + numMov,Toast.LENGTH_LONG).show();
            for(int i=0; i<tablero.getChildCount(); i++){
                botones[i/4][i%4].setClickable(false);//Se inhabilitan todos los botones
            }
            timer.cancel();
            BRevolver.setClickable(false);
            BPausar.setClickable(false);
            guardarDatos();

            Intent intent = new Intent(Tablero.this,PantallaGanador.class);
            intent.putExtra("Nombre",nombre);
            intent.putExtra("dispGanadora",dispGanadora);
            intent.putExtra("numMov",numMov);
            intent.putExtra("tiempo",ContadorTiempo);
            startActivity(intent);

        }
    }

    //se guardan los datos con ayuda de la clase MiBase
    private void guardarDatos(){
        miBase = new MiBase(Tablero.this);
        miBase.setUltimoMov(numMov);
        miBase.setUltimoTiempo(ContadorTiempo);
        if(miBase.getMejorMov() != 0){
            if(miBase.getMejorMov() > numMov)//Si en la ultima partida se usaron menos movimientos, se guarda dicha cantidad
                miBase.setMejorMov(numMov);
        }
        else
            miBase.setMejorMov(numMov);


        if(miBase.getMejorTiempo() != 0){
            if(miBase.getMejorTiempo() > ContadorTiempo)//Si en la ultima partida se usaron menos movimientos, se guarda dicha cantidad
                miBase.setMejorTiempo(ContadorTiempo);
        }
        else
            miBase.setMejorTiempo(ContadorTiempo);

    }

    //Se devuelve un codigo de respuesta que indicará al activity principal que debe actualizar el dato
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(Principal.CODIGO_RESPUESTA);
    }

    //Se revuelven las piezas nuevamente
    public void Revolver(View view){
        ContadorTiempo = 0;
        TVCountTiempo.setText("Tiempo: 00:00:00");
        numMov = 0;
        TVMovimientos.setText("Movimientos: 0");
        GenerarNumeros();
        CargarDatosAVistas();
    }

    //Se detiene el tiempo, se inhabilita el tableto y se cambia el mensaje del botón Pausar
    public void Pausar(View view){
        if(TiempoCorriendo){
            timer.cancel();
            BPausar.setText("Reanudar");
            TiempoCorriendo = false;

            for(int i=0; i<tablero.getChildCount(); i++){
                botones[i/4][i%4].setClickable(false);
            }
        }
        else{
            IniciarTemporizador();
            BPausar.setText("Pausar");

            for(int i=0; i<tablero.getChildCount(); i++){
                botones[i/4][i%4].setClickable(true);
            }
        }

    }
}