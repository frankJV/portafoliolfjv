package com.geb.taken;

import android.content.Context;
import android.content.SharedPreferences;

public class MiBase {
    public static final String SHARED_PREF = "sharedPref";
    public static final String ULTIMO_MOV = "ultimoMov";
    public static final String ULTIMO_TIEMPO = "ultimoTiempo";
    public static final String MEJOR_MOV = "mejorMov";
    public static final String MEJOR_TIEMPO = "mejorTiempo";
    private SharedPreferences preferencias; //La API permite guardar una colección pequeña de pares clave-valor
    private SharedPreferences.Editor editor;

    public MiBase(Context context){
        preferencias = context.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        editor = preferencias.edit();
    }

    //Se guarda la cantidad de movimientos de la ultima partida jugada
    public void setUltimoMov(int mov){
        editor.putInt(ULTIMO_MOV,mov).commit();
    }

    //Se obtiene la cantidad de movimientos de la ultima partida jugada
    public int getUltimoMov(){
        return preferencias.getInt(ULTIMO_MOV, 0);
    }

    //Se guarda la cantidad de movimientos de la ultima partida jugada
    public void setUltimoTiempo(int segundos){
        editor.putInt(ULTIMO_TIEMPO,segundos).commit();
    }

    //Se obtiene la cantidad de movimientos de la ultima partida jugada
    public int getUltimoTiempo(){
        return preferencias.getInt(ULTIMO_TIEMPO, 0);
    }

    //Se Guarda la menor cantidad de movimientos
    public void setMejorMov(int mov){
        editor.putInt(MEJOR_MOV,mov).commit();
    }

    //Se obtiene la menor cantidad de movimientos
    public int getMejorMov(){
        return preferencias.getInt(MEJOR_MOV, 0);
    }

    //Se guarda el mejor tiempo en segundos
    public void setMejorTiempo(int segundos){
        editor.putInt(MEJOR_TIEMPO,segundos).commit();
    }

    //Se obtiene el mejor tiempo en segundos
    public int getMejorTiempo(){
        return preferencias.getInt(MEJOR_TIEMPO, 0);
    }
}
