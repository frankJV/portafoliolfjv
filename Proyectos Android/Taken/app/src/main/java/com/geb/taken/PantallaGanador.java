package com.geb.taken;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

public class PantallaGanador extends AppCompatActivity {
    private String nombre;
    private String dispGanadora;
    private int numMov;
    private int tiempo;
    private TextView txtFelicidades;
    private TextView txtDispGandora;
    private TextView txtNumMov;
    private TextView txtTiempo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Se obtienen las medidas de la ventana
        DisplayMetrics medidasVentana = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(medidasVentana);

        int ancho = medidasVentana.widthPixels;
        int alto = medidasVentana.heightPixels;

        getWindow().setLayout((int)(ancho * 0.75), (int) (alto * 0.75));

        //Se obtienen los datos de las anteriores activities
        setContentView(R.layout.activity_pantalla_ganador);
        Bundle bundle = getIntent().getExtras();
        nombre = bundle.getString("Nombre");
        dispGanadora = bundle.getString("dispGanadora");
        numMov = bundle.getInt("numMov");
        tiempo = bundle.getInt("tiempo");

        txtFelicidades = findViewById(R.id.Felicidades);
        txtDispGandora = findViewById(R.id.DisposicionWin);
        txtNumMov = findViewById(R.id.NumMov);
        txtTiempo = findViewById(R.id.tiempoEmpleado);

        txtFelicidades.setText("Felicidades " + nombre + "\n Has ganado el juego");
        txtDispGandora.setText("Disposición ganadora: " + dispGanadora);
        txtNumMov.setText("Número de movimientos: " + String.valueOf(numMov));

        int segundo = tiempo % 60;
        int hora = tiempo /3600;
        int minuto = (tiempo - hora * 3600) / 60;

        txtTiempo.setText(String.format("Tiempo empleado: %02d:%02d:%02d", hora, minuto, segundo));
    }

    public void volver_Inicio(View view) {
        Intent intent = new Intent(PantallaGanador.this,Principal.class);
        intent.putExtra("Nombre",nombre);
        startActivity(intent);
        finish();
    }
}