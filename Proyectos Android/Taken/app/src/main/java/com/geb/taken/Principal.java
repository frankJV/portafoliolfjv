package com.geb.taken;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Principal extends AppCompatActivity {

    public static final int CODIGO_RESPUESTA = 1;

    private Button BIniciarJuego;
    private String nombre;
    private TextView BBienvenida;
    private TextView txtUltimoMov;
    private TextView txtUltimoTiempo;
    private TextView txtMejorMov;
    private TextView txtMejorTiempo;
    private MiBase miBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        getSupportActionBar().hide();

        Bundle bundle = getIntent().getExtras();
        nombre = bundle.getString("Nombre");

        BIniciarJuego = findViewById(R.id.Iniciar);
        BBienvenida = findViewById(R.id.Bienvenida);
        txtUltimoMov = findViewById(R.id.UltimoConteo);
        txtUltimoTiempo = findViewById(R.id.UltimoTiempo);
        txtMejorMov = findViewById(R.id.MejorCantMovimientos);
        txtMejorTiempo = findViewById(R.id.MejorTiempo);

        BBienvenida.setText("Bienvenido: " + nombre);

        miBase = new MiBase(this);

        cargarDatos();


    }

    //Se colocan las estadisticas del juego
    public void cargarDatos(){
        txtUltimoMov.setText(String.valueOf(miBase.getUltimoMov()));
        txtMejorMov.setText(String.valueOf(miBase.getMejorMov()));

        //Se convierten los segundos a un formato hh:mm::ss
        int ultimoTiempo = miBase.getUltimoTiempo();
        int ultimoSegundo = ultimoTiempo % 60;
        int ultimaHora = ultimoTiempo /3600;
        int ultimoMinuto = (ultimoTiempo - ultimaHora * 3600) / 60;
        txtUltimoTiempo.setText(String.format("%02d:%02d:%02d", ultimaHora, ultimoMinuto, ultimoSegundo));

        int mejorTiempo = miBase.getMejorTiempo();
        int mejorSegundo = mejorTiempo % 60;
        int mejorHora = mejorTiempo /3600;
        int mejorMinuto = (mejorTiempo - mejorHora * 3600) / 60;
        txtMejorTiempo.setText(String.format("%02d:%02d:%02d", mejorHora, mejorMinuto, mejorSegundo));
    }

    public void Iniciar_Juego(View view) {
        Intent intent = new Intent(Principal.this,Tablero.class);
        intent.putExtra("Nombre",nombre);
        startActivityForResult(intent,CODIGO_RESPUESTA);
    }

    //En caso de que el codigo de respuesta sea de 1, se guardaran las puntuaciones y tiempos en nuestro objeto de MiBase
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CODIGO_RESPUESTA){
            txtUltimoMov.setText(String.valueOf(miBase.getUltimoMov()));
            txtMejorMov.setText(String.valueOf(miBase.getMejorMov()));

            int ultimoTiempo = miBase.getUltimoTiempo();
            int ultimoSegundo = ultimoTiempo % 60;
            int ultimaHora = ultimoTiempo /3600;
            int ultimoMinuto = (ultimoTiempo - ultimaHora * 3600) / 60;
            txtUltimoTiempo.setText(String.format("%02d:%02d:%02d", ultimaHora, ultimoMinuto, ultimoSegundo));

            int mejorTiempo = miBase.getUltimoTiempo();
            int mejorSegundo = mejorTiempo % 60;
            int mejorHora = mejorTiempo /3600;
            int mejorMinuto = (mejorTiempo - mejorHora * 3600) / 60;
            txtMejorTiempo.setText(String.format("%02d:%02d:%02d", mejorHora, mejorMinuto, mejorSegundo));
        }
    }

    public void AcercaDe(View view){
        Intent intent = new Intent(Principal.this,AcercaDe.class);
        startActivity(intent);
    }
}