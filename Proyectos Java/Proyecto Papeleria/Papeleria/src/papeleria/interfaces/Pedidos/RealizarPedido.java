/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papeleria.interfaces.Pedidos;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import papeleria.interfaces.PantallaInicio;


public final class RealizarPedido extends javax.swing.JFrame {

    /**
     * Creates new form RealizarPedido
     */
    
    Connection con = null;
    Statement stmt = null;
    PreparedStatement ps;
    
    Date fecha1;
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
    
    
    String titulos[] = {"ID", "Cantidad", "Nombre", "Descripción", "Precio Unitario", "Importe"};
    String fila[] = new String[6];
    DefaultTableModel modelo;
    
    public RealizarPedido() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        
        fecha1 = new Date();
        Date  DatFecha = fecha1;
        String utilFecha = formato.format(DatFecha);
        obtenerEmpleados();
        obtenerProveedor();
        obtenerModelo();

        this.fecha.setText(utilFecha);
    }

    
     public void obtenerEmpleados(){
        try {

            String url = "jdbc:mysql://localhost:3306/Papeleria";
            String usuario = "root";
            String contraseña = "";
            String elim = "";

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(url, usuario, contraseña);
            if (con != null) {
                System.out.println("Se ha establecido una conexion a la base de datos" + "\n" + url);
            }

            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Empleado");


            while (rs.next()) {
                this.IdEmpleado.addItem(rs.getString("IdEmpleado") + " - " + rs.getString("Nombre"));
            }
                       
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            System.out.println("Error al consultar datos " + e.getMessage());
        }
    }
     
     public void obtenerProveedor(){
        try {

            String url = "jdbc:mysql://localhost:3306/Papeleria";
            String usuario = "root";
            String contraseña = "";
            String elim = "";

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(url, usuario, contraseña);
            if (con != null) {
                System.out.println("Se ha establecido una conexion a la base de datos" + "\n" + url);
            }

            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Proveedor");


            while (rs.next()) {
                this.IdProveedor1.addItem(rs.getString("IdProveedor") + " - " + rs.getString("NombreComercial"));
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            System.out.println("Error al consultar datos " + e.getMessage());
        }
    }
     
     public int obtenerId(String id){
         String aux="";
         int idF;
         
         for(int i = 0; i<id.length(); i++){
                    if(id.charAt(i) != ' '){
                        aux += id.charAt(i);
                    }
                    else{
                        idF = Integer.parseInt(aux);
                        return idF;
                    }
                }
         idF = Integer.parseInt(aux);
         return idF;
     }
     
     public void obtenerModelo(){
         modelo = new DefaultTableModel(null, titulos);
            Pedido.setModel(modelo);
            
            TableColumn id = Pedido.getColumn("ID");
            id.setMaxWidth(100);
            TableColumn can = Pedido.getColumn("Cantidad");
            can.setMaxWidth(100);
            TableColumn nom = Pedido.getColumn("Nombre");
            nom.setMaxWidth(177);
            TableColumn des = Pedido.getColumn("Descripción");
            des.setMaxWidth(360);
            TableColumn PU = Pedido.getColumn("Precio Unitario");
            PU.setMaxWidth(125);
            TableColumn imp = Pedido.getColumn("Importe");
            imp.setMaxWidth(125);
     }
    
    public void actualizarTotal(){
        float total = 0, p;
        if(this.Pedido.getRowCount() == 0){
            this.TotalV.setText("-----");
        }        
        
        else if(Pedido.getRowCount()>0){
            for(int i = 0; i< Pedido.getRowCount(); i++){
                p = Float.parseFloat(Pedido.getValueAt(i, 5).toString());
                total += p;
            }
            this.TotalV.setText(String.valueOf(String.format("%.2f", total)));
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        NombreC = new javax.swing.JLabel();
        fecha = new javax.swing.JLabel();
        NombreC1 = new javax.swing.JLabel();
        IdEmpleado = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        Pedido = new javax.swing.JTable();
        Elimina = new javax.swing.JButton();
        Consulta = new javax.swing.JLabel();
        ConsultaPro = new javax.swing.JTextField();
        Buscar = new javax.swing.JButton();
        IngresaPedido = new javax.swing.JButton();
        NombreC2 = new javax.swing.JLabel();
        TotalV = new javax.swing.JLabel();
        IdProveedor1 = new javax.swing.JComboBox<>();
        NombreC3 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        ConsultaPedido = new javax.swing.JButton();
        nuevoPedido = new javax.swing.JButton();
        Regresar2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 153, 102));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Realizar Pedido");

        NombreC.setBackground(new java.awt.Color(0, 0, 0));
        NombreC.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        NombreC.setText("Fecha");

        fecha.setBackground(new java.awt.Color(0, 0, 0));
        fecha.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        fecha.setText("DD/MM/AAAA");

        NombreC1.setBackground(new java.awt.Color(0, 0, 0));
        NombreC1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        NombreC1.setText("Id del Empleado: ");

        IdEmpleado.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        IdEmpleado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Seleccionar--" }));
        IdEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IdEmpleadoActionPerformed(evt);
            }
        });

        Pedido.setAutoCreateRowSorter(true);
        Pedido.setBackground(new java.awt.Color(102, 255, 102));
        Pedido.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        Pedido.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Id", "Cantidad", "Nombre", "Descripción", "Precio Unitario", "Importe"
            }
        ));
        Pedido.setFocusable(false);
        Pedido.setGridColor(new java.awt.Color(0, 0, 0));
        Pedido.setSelectionBackground(new java.awt.Color(225, 225, 225));
        Pedido.setSelectionForeground(new java.awt.Color(0, 0, 0));
        Pedido.setShowVerticalLines(false);
        Pedido.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                PedidoAncestorAdded(evt);
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        Pedido.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                PedidoFocusLost(evt);
            }
        });
        Pedido.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PedidoMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                PedidoMouseExited(evt);
            }
        });
        Pedido.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                PedidoInputMethodTextChanged(evt);
            }
        });
        Pedido.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                PedidoPropertyChange(evt);
            }
        });
        Pedido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PedidoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                PedidoKeyReleased(evt);
            }
        });
        Pedido.addVetoableChangeListener(new java.beans.VetoableChangeListener() {
            public void vetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {
                PedidoVetoableChange(evt);
            }
        });
        jScrollPane1.setViewportView(Pedido);

        Elimina.setBackground(new java.awt.Color(255, 255, 255));
        Elimina.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Elimina.setText("Eliminar");
        Elimina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminaActionPerformed(evt);
            }
        });

        Consulta.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Consulta.setText("Ingresa el id del producto:");

        ConsultaPro.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        ConsultaPro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConsultaProActionPerformed(evt);
            }
        });

        Buscar.setBackground(new java.awt.Color(255, 255, 255));
        Buscar.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Buscar.setText("Buscar");
        Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuscarActionPerformed(evt);
            }
        });

        IngresaPedido.setBackground(new java.awt.Color(255, 255, 255));
        IngresaPedido.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        IngresaPedido.setText("INGRESAR PEDIDO");
        IngresaPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IngresaPedidoActionPerformed(evt);
            }
        });

        NombreC2.setBackground(new java.awt.Color(0, 0, 0));
        NombreC2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        NombreC2.setText("TOTAL:");

        TotalV.setBackground(new java.awt.Color(0, 0, 0));
        TotalV.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        TotalV.setText("-----");

        IdProveedor1.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        IdProveedor1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Seleccionar--" }));
        IdProveedor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IdProveedor1ActionPerformed(evt);
            }
        });

        NombreC3.setBackground(new java.awt.Color(0, 0, 0));
        NombreC3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        NombreC3.setText("Id del Proveedor: ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(129, 129, 129)
                .addComponent(Consulta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ConsultaPro, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Buscar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(IngresaPedido)
                .addGap(25, 25, 25))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(NombreC)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fecha)
                                .addGap(42, 42, 42)
                                .addComponent(NombreC3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(IdProveedor1, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(NombreC1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(IdEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 971, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(Elimina)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(NombreC2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TotalV, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NombreC)
                    .addComponent(NombreC1)
                    .addComponent(fecha)
                    .addComponent(IdEmpleado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(NombreC3)
                    .addComponent(IdProveedor1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(NombreC2)
                            .addComponent(TotalV))
                        .addGap(50, 50, 50)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ConsultaPro)
                            .addComponent(Buscar)
                            .addComponent(IngresaPedido)
                            .addComponent(Consulta))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Elimina)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        jPanel3.setBackground(new java.awt.Color(102, 153, 255));

        jLabel5.setBackground(new java.awt.Color(0, 0, 0));
        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("PEDIDOS");

        ConsultaPedido.setBackground(new java.awt.Color(153, 0, 0));
        ConsultaPedido.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        ConsultaPedido.setForeground(new java.awt.Color(255, 255, 255));
        ConsultaPedido.setText("Consultar");
        ConsultaPedido.setBorder(null);
        ConsultaPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConsultaPedidoActionPerformed(evt);
            }
        });

        nuevoPedido.setBackground(new java.awt.Color(153, 0, 0));
        nuevoPedido.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        nuevoPedido.setForeground(new java.awt.Color(255, 255, 255));
        nuevoPedido.setText("Nuevo Pedido");
        nuevoPedido.setBorder(null);
        nuevoPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuevoPedidoActionPerformed(evt);
            }
        });

        Regresar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Regresar.png"))); // NOI18N
        Regresar2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Regresar2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(Regresar2)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(153, 153, 153)
                        .addComponent(nuevoPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 206, Short.MAX_VALUE)
                        .addComponent(ConsultaPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(139, 139, 139))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 11, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Regresar2)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ConsultaPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nuevoPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void IdEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IdEmpleadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IdEmpleadoActionPerformed

    private void PedidoAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_PedidoAncestorAdded

    }//GEN-LAST:event_PedidoAncestorAdded

    private void PedidoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_PedidoFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_PedidoFocusLost

    @SuppressWarnings("empty-statement")
    private void PedidoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PedidoMouseClicked

        float importe;
        int filaSeleccionada = Pedido.getSelectedRow();
        String cantidad= (String) Pedido.getValueAt(filaSeleccionada, 1);
        String precio= (String) Pedido.getValueAt(filaSeleccionada, 4);
        if(Integer.parseInt(cantidad) <= 0){
            javax.swing.JOptionPane.showMessageDialog(this, "Favor de ingresar cantidades mayores a 0", "ADVERTENCIA", javax.swing.JOptionPane.WARNING_MESSAGE);
            Pedido.getModel().setValueAt(1, filaSeleccionada, 1);
        }
        else{
            importe = Integer.parseInt(cantidad) * Float.parseFloat(precio);
            Pedido.getModel().setValueAt(String.format("%.2f", importe), filaSeleccionada, 5);
        }
        

        this.actualizarTotal();;
    }//GEN-LAST:event_PedidoMouseClicked

    private void PedidoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PedidoMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_PedidoMouseExited

    private void PedidoInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_PedidoInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_PedidoInputMethodTextChanged

    private void PedidoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_PedidoPropertyChange
        // TODO add your handling code here:

    }//GEN-LAST:event_PedidoPropertyChange

    private void PedidoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PedidoKeyPressed

    }//GEN-LAST:event_PedidoKeyPressed

    private void PedidoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PedidoKeyReleased

    }//GEN-LAST:event_PedidoKeyReleased

    private void PedidoVetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {//GEN-FIRST:event_PedidoVetoableChange

    }//GEN-LAST:event_PedidoVetoableChange

    private void EliminaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminaActionPerformed
        int filaSeleccionada = Pedido.getSelectedRow();
        modelo.removeRow(filaSeleccionada);
        this.actualizarTotal();
    }//GEN-LAST:event_EliminaActionPerformed

    private void Regresar2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Regresar2MouseClicked

        PantallaInicio abrir = new PantallaInicio();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_Regresar2MouseClicked

    private void ConsultaProActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConsultaProActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ConsultaProActionPerformed

    private void BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuscarActionPerformed
        try {
            stmt = con.createStatement();
            int idB;
            String nombreB = this.ConsultaPro.getText();
            ResultSet rs;

            idB = Integer.parseInt(nombreB);
            rs = stmt.executeQuery("select * from Producto WHERE idProducto = " + idB);

            while (rs.next()) {
                fila[0] = rs.getString("IdProducto");
                fila[1] = "1";
                fila[2] = rs.getString("Nombre");
                fila[3] = rs.getString("Descripcion");
                fila[4] = rs.getString("precioProveedor");
                fila[5] = rs.getString("precioProveedor");

                //importe = Float.parseFloat(fila[4]) * Integer.parseInt(fila[1]);

                modelo.addRow(fila);
            }
            Pedido.setModel(modelo);

            TableColumn id = Pedido.getColumn("ID");
            id.setMaxWidth(100);
            TableColumn can = Pedido.getColumn("Cantidad");
            can.setMaxWidth(100);
            TableColumn nom = Pedido.getColumn("Nombre");
            nom.setMaxWidth(177);
            TableColumn des = Pedido.getColumn("Descripción");
            des.setMaxWidth(360);
            TableColumn PU = Pedido.getColumn("Precio Unitario");
            PU.setMaxWidth(125);
            TableColumn imp = Pedido.getColumn("Importe");
            imp.setMaxWidth(125);

            this.actualizarTotal();

        } catch (NumberFormatException | SQLException e) {
            System.out.println("Error al consultar datos " + e.getMessage());
        }
    }//GEN-LAST:event_BuscarActionPerformed

    private void IngresaPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IngresaPedidoActionPerformed
        try {
            int seleccion = JOptionPane.showOptionDialog(this,"¿Esta seguro de que desea ingresar el Pedido?",
            "ADVERTENCIA",JOptionPane.WARNING_MESSAGE,
            JOptionPane.WARNING_MESSAGE,null,// null para icono por defecto.
            new Object[] { "Aceptar", "Cancelar"},"Aceptar");
            
            if(seleccion == 0){
                String url = "jdbc:mysql://localhost:3306/Papeleria";
                String usuario = "root";
                String contraseña = "";

                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con = DriverManager.getConnection(url, usuario, contraseña);
                if (con != null) {
                    System.out.println("Se ha establecido una conexión a la base de datos "
                        + "\n " + url);

                    stmt = con.createStatement();

                    //Agregar valores a la tabla Pedido
                    String SSQL = "INSERT INTO Pedido (Fecha, idProveedor, idEmpleado) "
                    + "VALUES (?, ?, ?)";

                    String Fecha = this.fecha.getText();
                    String Emp = this.IdEmpleado.getModel().getSelectedItem().toString();
                    String Prov = this.IdProveedor1.getModel().getSelectedItem().toString();
                    int idEmp;
                    int idProv;

                    idEmp = obtenerId(Emp);

                    idProv = obtenerId(Prov);


                    ps = (PreparedStatement) con.prepareStatement(SSQL);
                    ps.setString(1, Fecha);
                    ps.setInt(2, idProv);
                    ps.setInt(3, idEmp);

                    int res = ps.executeUpdate();

                    //Agregar valores a la tabla ProPed
                    String SSQL1 = "INSERT INTO ProPed (IdPedido, IdProducto, Cantidad) "
                    + "VALUES (?, ?, ?)";
                    int idProducto;
                    int idPedido1 = 0;
                    int Cantidad;

                    stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery("select idPedido from Pedido");

                   // modelo = new DefaultTableModel(null, titulos);

                    while (rs.next()) {
                        idPedido1 = rs.getInt("idPedido");
                    }

                    for(int i=0; i<Pedido.getModel().getRowCount(); i++){

                        idProducto = Integer.parseInt(Pedido.getValueAt(i, 0).toString());
                        Cantidad = Integer.parseInt(Pedido.getValueAt(i, 1).toString());
                        ps = (PreparedStatement) con.prepareStatement(SSQL1);
                        ps.setInt(1, idPedido1);
                        ps.setInt(2, idProducto);
                        ps.setInt(3, Cantidad);

                        ps.executeUpdate();
                    }

                    if(res > 0){
                        javax.swing.JOptionPane.showMessageDialog(this, "El pedido se registro satisfactoriamente", "AVISO", javax.swing.JOptionPane.INFORMATION_MESSAGE);
                        System.out.println("Los valores han sido agregados a la base de datos ");
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Error al registrar Pedido");
                    }

                }
                obtenerModelo();
                this.TotalV.setText("-----");
            }
            
            

        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RealizarPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        
    }//GEN-LAST:event_IngresaPedidoActionPerformed

    private void IdProveedor1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IdProveedor1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IdProveedor1ActionPerformed

    private void ConsultaPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConsultaPedidoActionPerformed
        // TODO add your handling code here:
        ConsultaPedido consulta = new ConsultaPedido();
        consulta.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_ConsultaPedidoActionPerformed

    private void nuevoPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuevoPedidoActionPerformed
        RealizarPedido inserta = new RealizarPedido();
        inserta.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_nuevoPedidoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RealizarPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new RealizarPedido().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Buscar;
    private javax.swing.JLabel Consulta;
    private javax.swing.JButton ConsultaPedido;
    private javax.swing.JTextField ConsultaPro;
    private javax.swing.JButton Elimina;
    private javax.swing.JComboBox<String> IdEmpleado;
    private javax.swing.JComboBox<String> IdProveedor1;
    private javax.swing.JButton IngresaPedido;
    private javax.swing.JLabel NombreC;
    private javax.swing.JLabel NombreC1;
    private javax.swing.JLabel NombreC2;
    private javax.swing.JLabel NombreC3;
    private javax.swing.JTable Pedido;
    private javax.swing.JLabel Regresar2;
    private javax.swing.JLabel TotalV;
    private javax.swing.JLabel fecha;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton nuevoPedido;
    // End of variables declaration//GEN-END:variables
}
