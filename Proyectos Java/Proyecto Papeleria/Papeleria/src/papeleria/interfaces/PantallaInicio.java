/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papeleria.interfaces;

import javax.swing.JOptionPane;
import papeleria.interfaces.Cliente.Principal;
import papeleria.interfaces.Cortes.PrincipalCortes;
import papeleria.interfaces.Empleado.PrincipalE;
import papeleria.interfaces.Factura.ImprimirFactura;
import papeleria.interfaces.Inventario.PrincipalInv;
import papeleria.interfaces.Pedidos.PrincipalPed;
import papeleria.interfaces.Producto.PrincipalPro;
import papeleria.interfaces.Proveedor.PrincipalP;
import papeleria.interfaces.ReporteVentas.Reportes;
import papeleria.interfaces.Usuario.PrincipalUsuario;
import papeleria.interfaces.Ventas.RealizarVenta;

public final class PantallaInicio extends javax.swing.JFrame {

    /**
     * Creates new form PantallaInicio
     */
    public PantallaInicio() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.Rol.setVisible(false);
        
        Login login = new Login();
        this.Usuario.setText(Login.User);
        this.Rol.setText(Login.Rol);
        
        ComprobarRol();
    }
    
    public void ComprobarRol(){
        //System.out.println(this.Rol.getText());
        if("Empleado".equals(this.Rol.getText())){
            this.Inventarios1.setEnabled(false);
            this.Inventarios.setEnabled(false);
            this.Empleados1.setEnabled(false);
            this.Empleados.setEnabled(false); 
            this.AUsuarios.setEnabled(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        Usuario = new javax.swing.JLabel();
        Usuario1 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        salir1 = new javax.swing.JLabel();
        Salir = new javax.swing.JLabel();
        Inventarios1 = new javax.swing.JLabel();
        Pedidos1 = new javax.swing.JLabel();
        Ventas1 = new javax.swing.JLabel();
        Productos1 = new javax.swing.JLabel();
        Productos = new javax.swing.JLabel();
        Ventas = new javax.swing.JLabel();
        Pedidos = new javax.swing.JLabel();
        Inventarios = new javax.swing.JLabel();
        AUsuarios = new javax.swing.JLabel();
        Facturas1 = new javax.swing.JLabel();
        Facturas = new javax.swing.JLabel();
        Empleados1 = new javax.swing.JLabel();
        Empleados = new javax.swing.JLabel();
        Clientes1 = new javax.swing.JLabel();
        Clientes = new javax.swing.JLabel();
        Proveedores = new javax.swing.JLabel();
        Facturas2 = new javax.swing.JLabel();
        Cortes = new javax.swing.JLabel();
        Cortes1 = new javax.swing.JLabel();
        Reportes = new javax.swing.JLabel();
        Reportes2 = new javax.swing.JLabel();
        Rol = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(153, 102, 255));

        Usuario.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        Usuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Usuario.setText("Usuario");

        Usuario1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/login.png"))); // NOI18N

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("PAPELERÍA \" EL MUNDI\"");

        salir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cerrarsesion.jpg"))); // NOI18N
        salir1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salir1MouseClicked(evt);
            }
        });

        Salir.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        Salir.setText("SALIR");
        Salir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SalirMouseClicked(evt);
            }
        });

        Inventarios1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/inventarios.png"))); // NOI18N
        Inventarios1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Inventarios1MouseClicked(evt);
            }
        });

        Pedidos1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pedidos.png"))); // NOI18N
        Pedidos1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Pedidos1MouseClicked(evt);
            }
        });

        Ventas1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ventas.png"))); // NOI18N
        Ventas1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Ventas1MouseClicked(evt);
            }
        });

        Productos1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/productos.jpg"))); // NOI18N
        Productos1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Productos1MouseClicked(evt);
            }
        });

        Productos.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        Productos.setText("PRODUCTOS");
        Productos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ProductosMouseClicked(evt);
            }
        });

        Ventas.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        Ventas.setText("VENTAS");
        Ventas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                VentasMouseClicked(evt);
            }
        });

        Pedidos.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        Pedidos.setText("PEDIDOS");
        Pedidos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                PedidosMouseClicked(evt);
            }
        });

        Inventarios.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        Inventarios.setText("INVENTARIOS");
        Inventarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                InventariosMouseClicked(evt);
            }
        });

        AUsuarios.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        AUsuarios.setForeground(new java.awt.Color(204, 0, 0));
        AUsuarios.setText("ADMINISTRAR USUARIOS");
        AUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AUsuariosMouseClicked(evt);
            }
        });

        Facturas1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/facturas.png"))); // NOI18N
        Facturas1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Facturas1MouseClicked(evt);
            }
        });

        Facturas.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        Facturas.setText("FACTURAS");
        Facturas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                FacturasMouseClicked(evt);
            }
        });

        Empleados1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/usuario.png"))); // NOI18N
        Empleados1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Empleados1MouseClicked(evt);
            }
        });

        Empleados.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        Empleados.setText("EMPLEADOS");
        Empleados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                EmpleadosMouseClicked(evt);
            }
        });

        Clientes1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/empleados.png"))); // NOI18N
        Clientes1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Clientes1MouseClicked(evt);
            }
        });

        Clientes.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        Clientes.setText("CLIENTES");
        Clientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ClientesMouseClicked(evt);
            }
        });

        Proveedores.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        Proveedores.setText("PROVEEDORES");
        Proveedores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ProveedoresMouseClicked(evt);
            }
        });

        Facturas2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/proveedores.png"))); // NOI18N
        Facturas2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Facturas2MouseClicked(evt);
            }
        });

        Cortes.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        Cortes.setText("CORTES");
        Cortes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CortesMouseClicked(evt);
            }
        });

        Cortes1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cortes.jpg"))); // NOI18N
        Cortes1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Cortes1MouseClicked(evt);
            }
        });

        Reportes.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        Reportes.setText("REPORTES");
        Reportes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ReportesMouseClicked(evt);
            }
        });

        Reportes2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/reporteventas.png"))); // NOI18N
        Reportes2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Reportes2MouseClicked(evt);
            }
        });

        Rol.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        Rol.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Rol.setText("Rol");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(Productos))
                            .addComponent(Productos1)
                            .addComponent(Clientes1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addComponent(Clientes))
                            .addComponent(Facturas1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addComponent(Facturas)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(56, 56, 56)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Ventas1)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(33, 33, 33)
                                        .addComponent(Ventas))
                                    .addComponent(Cortes1)
                                    .addComponent(Facturas2)
                                    .addComponent(Proveedores, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(92, 92, 92)
                                .addComponent(Cortes)))
                        .addGap(44, 44, 44)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(AUsuarios, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(Inventarios1)
                                        .addGap(13, 13, 13))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(Empleados1)
                                            .addComponent(Pedidos1))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(Reportes2))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(0, 26, Short.MAX_VALUE)
                                                .addComponent(Pedidos)
                                                .addGap(109, 109, 109))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(10, 10, 10)
                                                .addComponent(Empleados)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(10, 10, 10)
                                                .addComponent(Reportes))
                                            .addComponent(Inventarios))))
                                .addGap(13, 13, 13))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Usuario, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Rol, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Usuario1)
                        .addGap(92, 92, 92)
                        .addComponent(jLabel10)
                        .addGap(52, 52, 52)
                        .addComponent(salir1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Salir)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(Usuario1)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel10)
                                    .addGap(35, 35, 35))
                                .addComponent(salir1))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(Usuario)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Rol))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addComponent(Salir)))
                .addGap(37, 37, 37)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Productos1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Productos))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Ventas1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Ventas))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Pedidos1)
                            .addComponent(Inventarios1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Pedidos)
                            .addComponent(Inventarios))))
                .addGap(37, 37, 37)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(Clientes1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Clientes))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Reportes2)
                            .addComponent(Empleados1)
                            .addComponent(Cortes1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Empleados)
                            .addComponent(Cortes)
                            .addComponent(Reportes))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Facturas1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Facturas2, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Facturas)
                            .addComponent(Proveedores))
                        .addGap(24, 24, 24))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(AUsuarios)
                        .addContainerGap())))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Productos1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Productos1MouseClicked
        PrincipalPro abrir = new PrincipalPro();
        abrir.setVisible(true);
        this.setVisible(false);

    }//GEN-LAST:event_Productos1MouseClicked

    private void ProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ProductosMouseClicked
        PrincipalPro abrir = new PrincipalPro();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_ProductosMouseClicked

    private void Ventas1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Ventas1MouseClicked
        // TODO add your handling code here:
        RealizarVenta abrir = new RealizarVenta();
        abrir.setVisible(true);
        this.setVisible(false);  
    }//GEN-LAST:event_Ventas1MouseClicked

    private void VentasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_VentasMouseClicked
        // TODO add your handling code here:
        RealizarVenta abrir = new RealizarVenta();
        abrir.setVisible(true);
        this.setVisible(false);  
    }//GEN-LAST:event_VentasMouseClicked

    private void Clientes1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Clientes1MouseClicked
        // TODO add your handling code here:
        Principal abrir = new Principal();
        abrir.setVisible(true);
        this.setVisible(false);  
    }//GEN-LAST:event_Clientes1MouseClicked

    private void ClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ClientesMouseClicked
        // TODO add your handling code here:
        Principal abrir = new Principal();
        abrir.setVisible(true);
        this.setVisible(false);  
    }//GEN-LAST:event_ClientesMouseClicked

    private void Empleados1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Empleados1MouseClicked
        // TODO add your handling code here:  
        if(this.Inventarios1.isEnabled() == true){
        PrincipalE abrir = new PrincipalE();
        abrir.setVisible(true);
        this.setVisible(false);              
        }
    }//GEN-LAST:event_Empleados1MouseClicked

    private void EmpleadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_EmpleadosMouseClicked
        // TODO add your handling code here:
        if(this.Inventarios1.isEnabled() == true){
        PrincipalE abrir = new PrincipalE();
        abrir.setVisible(true);
        this.setVisible(false);              
        }
    }//GEN-LAST:event_EmpleadosMouseClicked

    private void ProveedoresMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ProveedoresMouseClicked
        // TODO add your handling code here:
        PrincipalP abrir = new PrincipalP();
        abrir.setVisible(true);
        this.setVisible(false);  
    }//GEN-LAST:event_ProveedoresMouseClicked

    private void salir1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salir1MouseClicked
        //Panel de Advertencia
        int seleccion = JOptionPane.showOptionDialog(this,"¿Desea cerrar sesión?",
        "ADVERTENCIA",JOptionPane.WARNING_MESSAGE,
        JOptionPane.WARNING_MESSAGE,null,// null para icono por defecto.
        new Object[] { "Aceptar", "Cancelar"},"Aceptar");
        
        if(seleccion == 0){
            Login abrir = new Login();
            abrir.setVisible(true);
            this.setVisible(false); 
        }
    }//GEN-LAST:event_salir1MouseClicked

    private void Facturas2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Facturas2MouseClicked
        // TODO add your handling code here:
        PrincipalP abrir = new PrincipalP();
        abrir.setVisible(true);
        this.setVisible(false);  
    }//GEN-LAST:event_Facturas2MouseClicked

    private void CortesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CortesMouseClicked
        // TODO add your handling code here:
        PrincipalCortes abrir = new PrincipalCortes();
        abrir.setVisible(true);
        this.setVisible(false); 
    }//GEN-LAST:event_CortesMouseClicked

    private void ReportesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ReportesMouseClicked
        Reportes abrir = new Reportes();
        abrir.setVisible(true);
        this.setVisible(false); 
    }//GEN-LAST:event_ReportesMouseClicked

    private void Cortes1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Cortes1MouseClicked
        // TODO add your handling code here:
        PrincipalCortes abrir = new PrincipalCortes();
        abrir.setVisible(true);
        this.setVisible(false);  
    }//GEN-LAST:event_Cortes1MouseClicked

    private void Reportes2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Reportes2MouseClicked
        Reportes abrir = new Reportes();
        abrir.setVisible(true);
        this.setVisible(false); 
    }//GEN-LAST:event_Reportes2MouseClicked

    private void SalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SalirMouseClicked
        //Panel de Advertencia
        int seleccion = JOptionPane.showOptionDialog(this,"¿Desea cerrar sesión?",
            "ADVERTENCIA",JOptionPane.WARNING_MESSAGE,
            JOptionPane.WARNING_MESSAGE,null,// null para icono por defecto.
            new Object[] { "Aceptar", "Cancelar"},"Aceptar");

        if(seleccion == 0){
            Login abrir = new Login();
            abrir.setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_SalirMouseClicked

    private void Pedidos1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Pedidos1MouseClicked
        PrincipalPed abrir = new PrincipalPed();
        abrir.setVisible(true);
        this.setVisible(false); 
    }//GEN-LAST:event_Pedidos1MouseClicked

    private void PedidosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_PedidosMouseClicked
        PrincipalPed abrir = new PrincipalPed();
        abrir.setVisible(true);
        this.setVisible(false); 
    }//GEN-LAST:event_PedidosMouseClicked

    private void Facturas1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Facturas1MouseClicked
        ImprimirFactura abrir = new ImprimirFactura();
        abrir.setVisible(true);
        this.setVisible(false); 
    }//GEN-LAST:event_Facturas1MouseClicked

    private void FacturasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FacturasMouseClicked
        ImprimirFactura abrir = new ImprimirFactura();
        abrir.setVisible(true);
        this.setVisible(false); 
    }//GEN-LAST:event_FacturasMouseClicked

    private void Inventarios1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Inventarios1MouseClicked
        PrincipalInv abrir = new PrincipalInv();
        abrir.setVisible(true);
        this.setVisible(false); 
    }//GEN-LAST:event_Inventarios1MouseClicked

    private void InventariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InventariosMouseClicked
        PrincipalInv abrir = new PrincipalInv();
        abrir.setVisible(true);
        this.setVisible(false); 
    }//GEN-LAST:event_InventariosMouseClicked

    private void AUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AUsuariosMouseClicked
        PrincipalUsuario abrir = new PrincipalUsuario();
        abrir.setVisible(true);
        this.setVisible(false); 
    }//GEN-LAST:event_AUsuariosMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PantallaInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PantallaInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PantallaInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PantallaInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new PantallaInicio().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AUsuarios;
    private javax.swing.JLabel Clientes;
    private javax.swing.JLabel Clientes1;
    private javax.swing.JLabel Cortes;
    private javax.swing.JLabel Cortes1;
    private javax.swing.JLabel Empleados;
    private javax.swing.JLabel Empleados1;
    private javax.swing.JLabel Facturas;
    private javax.swing.JLabel Facturas1;
    private javax.swing.JLabel Facturas2;
    private javax.swing.JLabel Inventarios;
    private javax.swing.JLabel Inventarios1;
    private javax.swing.JLabel Pedidos;
    private javax.swing.JLabel Pedidos1;
    private javax.swing.JLabel Productos;
    private javax.swing.JLabel Productos1;
    private javax.swing.JLabel Proveedores;
    private javax.swing.JLabel Reportes;
    private javax.swing.JLabel Reportes2;
    public javax.swing.JLabel Rol;
    private javax.swing.JLabel Salir;
    public javax.swing.JLabel Usuario;
    private javax.swing.JLabel Usuario1;
    private javax.swing.JLabel Ventas;
    private javax.swing.JLabel Ventas1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel salir1;
    // End of variables declaration//GEN-END:variables
}
