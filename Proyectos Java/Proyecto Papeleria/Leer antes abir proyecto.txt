El proyecto está desarrollado en la IDE de Apache NetBeans utilizando código Java.

Al momento de abrir el proyecto arrojará un mensaje de error diciendo que no encuentra un archivo en las librerías, dicho archivo es
el encargado de generar las facturas correspondientes a las compras realizadas en la papelería. Para añadir dicho archivo, se debe 
dirigir a la parte izquierda en donde se encuentran las carpetas del proyecto y dirigirse al apartado de librerías, dar clic derecho
y seleccionar la tercera opción “Add JAR/Folder” y dentro de la carpeta del proyecto seleccionar el archivo llamado “itextpdf-5.3.3”
Finalmente para generar la factura Se debe cambiar la ruta en la cual se guardará la factura, por lo que se debe dirigirse al paquete:
“papeleria.interfaces.Factura” y abrir el único archivo ahí y dirigirse a la parte de código, en la línea 272 y 415 Se deberá cambiar
la ruta a la que se prefiera. Cabe mencionar que se debe montar la base de datos que esta incluida en el folder del proyecto y que lleva
el nombre de “Papeleria.sql”.

En el inicio de sesión existen dos maneras de hacerlo, ya sea como dueño o empleado. A continuación, se añade un usuario de cada uno:
Dueño: 
	Usuario: QuecholJE
	Contraseña: 987654321
Empleado:
	Usuario: BrandonPB
	Contraseña: 123456789

By: José Eduardo Zamora Quechol, Luis Francisco Jiménez Viguras y Brandon Pilotzi Bautista.