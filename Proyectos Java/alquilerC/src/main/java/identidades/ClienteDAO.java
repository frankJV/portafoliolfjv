/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package identidades;

import conecciones.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Kama
 */
public class ClienteDAO {
    private static final String SQL_SELECT = "SELECT * FROM cliente";
    private static final String SQL_SELECT_INDIVIDUAL = "SELECT idCliente, Nombre, AP, AM, Telefono FROM cliente WHERE idCliente=?";
    private static final String SQL_INSERT = "INSERT INTO cliente(Nombre, AP, AM, Telefono) VALUES(?, ?, ?, ?)";
    private static final String SQL_INSERT_ID = "INSERT INTO cliente(idCliente, Nombre, AP, AM, Telefono) VALUES(?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE cliente SET Nombre=?, AP=?, AM=?, Telefono=? WHERE idCliente=?";
    private static final String SQL_DELETE = "DELETE FROM cliente WHERE idCliente=?";
    private static final String SQL_AUTO_INCREMENT = "SELECT MAX(idCliente) FROM cliente";
    
    public static int siguiente_id() {
        int id = 0;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_AUTO_INCREMENT);
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                id = rs.getInt(1) + 1;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }
        
        return id;
    }
    
    public static int NRegistros() {
        ArrayList<Cliente> clientes = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Cliente cliente = new Cliente(rs.getInt("idCliente"),
                        rs.getString("Nombre"),
                        rs.getString("AP"),
                        rs.getString("AM"),
                        (rs.getLong("Telefono")));
                clientes.add(cliente);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return clientes.size();
    }
    
    public static Cliente select_individual(String id) {
        Cliente cliente = null;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT_INDIVIDUAL);
            stmt.setString(1, id);//al orden de la instruccion de atributo
            rs = stmt.executeQuery();

            while (rs.next()) {
                cliente = new Cliente(rs.getInt("idCliente"),
                        rs.getString("Nombre"),
                        rs.getString("AP"),
                        rs.getString("AM"),
                        (rs.getLong("Telefono")));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return cliente;
    }
    
    public static ArrayList<Cliente> select() {
        ArrayList<Cliente> clientes = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Cliente cliente = new Cliente(rs.getInt("idCliente"),
                        rs.getString("Nombre"),
                        rs.getString("AP"),
                        rs.getString("AM"),
                        (rs.getLong("Telefono")));
                clientes.add(cliente);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return clientes;
    }
    
    public static int insert_id(Cliente cliente) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_INSERT_ID);
            
            stmt.setInt(1, cliente.getIdCliente());
            stmt.setString(2, cliente.getNombre());//al orden de la instruccion de atributo
            stmt.setString(3, cliente.getApellidoP());
            stmt.setString(4, cliente.getApellidoM());
            stmt.setLong(5, cliente.getTelefono());
            
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }

    public static int insert(Cliente cliente) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_INSERT);
            
            stmt.setString(1, cliente.getNombre());//al orden de la instruccion de atributo
            stmt.setString(2, cliente.getApellidoP());
            stmt.setString(3, cliente.getApellidoM());
            stmt.setLong(4, cliente.getTelefono());
            
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }

    public static int update(Cliente cliente) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_UPDATE);

            stmt.setString(1, cliente.getNombre());//al orden de la instruccion de atributo
            stmt.setString(2, cliente.getApellidoP());
            stmt.setString(3, cliente.getApellidoM());
            stmt.setLong(4, cliente.getTelefono());
            stmt.setInt(5, cliente.getIdCliente());

            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
    
    public static int delete(Cliente cliente) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_DELETE);
            stmt.setInt(1, cliente.getIdCliente());
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
}