/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package identidades;

import java.sql.Date;

/**
 *
 * @author Kama
 */
public class Informe {
    private int idInforme;
    private float totalPagar;
    private Date fecha;
    private int idCliente;
    private int idCoche;

    public Informe() {
    }

    public Informe(int idInforme, float totalPagar, Date fecha, int idCliente, int idCoche) {
        this.idInforme = idInforme;
        this.totalPagar = totalPagar;
        this.fecha = fecha;
        this.idCliente = idCliente;
        this.idCoche = idCoche;
    }

    public Informe(float totalPagar, Date fecha, int idCliente, int idCoche) {
        this.totalPagar = totalPagar;
        this.fecha = fecha;
        this.idCliente = idCliente;
        this.idCoche = idCoche;
    }

    public Informe(int idInforme) {
        this.idInforme = idInforme;
    }

    public int getIdInforme() {
        return idInforme;
    }

    public void setIdInforme(int idInforme) {
        this.idInforme = idInforme;
    }

    public float getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(float totalPagar) {
        this.totalPagar = totalPagar;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdCoche() {
        return idCoche;
    }

    public void setIdCoche(int idCoche) {
        this.idCoche = idCoche;
    }

    @Override
    public String toString() {
        return "Informe{" + "idInforme=" + idInforme + ", totalPagar=" + totalPagar + ", fecha=" + fecha + ", idCliente=" + idCliente + ", idCoche=" + idCoche + '}';
    }

}
