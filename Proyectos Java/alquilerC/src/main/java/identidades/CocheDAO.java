package identidades;

import conecciones.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Kama
 */
public class CocheDAO {
    private static final String SQL_SELECT = "SELECT * FROM coche";
    private static final String SQL_SELECT_INDIVIDUAL = "SELECT idCoche, Placa, Modelo, Marca, PrecioDias FROM coche WHERE idCoche=?";
    private static final String SQL_INSERT = "INSERT INTO coche(Placa, Modelo, Marca, PrecioDias) VALUES(?, ?, ?, ?)";
    private static final String SQL_INSERT_ID = "INSERT INTO coche(idCoche, Placa, Modelo, Marca, PrecioDias) VALUES(?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE coche SET Placa=?, Modelo=?, Marca=?, PrecioDias=? WHERE idCoche=?";
    private static final String SQL_DELETE = "DELETE FROM coche WHERE idCoche=?";
    private static final String SQL_AUTO_INCREMENT = "SELECT MAX(idCoche) FROM coche";
    
    public static int siguiente_id() {
        int id = 0;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_AUTO_INCREMENT);
            rs = stmt.executeQuery();
            
            while (rs.next()) {
                id = rs.getInt(1) + 1;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }
        
        return id;
    }
    
    public static int NRegistros() {
        ArrayList<Coche> Coche = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Coche coche = new Coche(rs.getInt("idCoche"),
                        rs.getString("Placa"),
                        rs.getString("Modelo"),
                        rs.getString("Marca"),
                        rs.getDouble("PrecioDias"));
                Coche.add(coche);
            }

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return Coche.size();
    }
    
    
    public static Coche select_individual(String id) {
        Coche coche = null;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT_INDIVIDUAL);
            stmt.setString(1, id);//al orden de la instruccion de atributo
            rs = stmt.executeQuery();

            while (rs.next()) {
                coche = new Coche(rs.getInt("idCoche"),
                        rs.getString("Placa"),
                        rs.getString("Modelo"),
                        rs.getString("Marca"),
                        (rs.getLong("PrecioDias")));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return coche;
    }          
    
    
    public static ArrayList<Coche> select() {
        ArrayList<Coche> Coche = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Coche coche = new Coche(rs.getInt("idCoche"),
                        rs.getString("Placa"),
                        rs.getString("Modelo"),
                        rs.getString("Marca"),
                        rs.getDouble("PrecioDias"));
                Coche.add(coche);
            }

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
            Conexion.close(rs);
        }

        return Coche;
    }

    public static int insert_id(Coche coche) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_INSERT_ID);
            
            stmt.setInt(1, coche.getIdCoche());
            stmt.setString(2, coche.getPlaca());//al orden de la instruccion de atributo
            stmt.setString(3, coche.getModelo());
            stmt.setString(4, coche.getMarca());
            stmt.setDouble(5, coche.getPrecioDias());
            
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
    
    public static int insert(Coche coche) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_INSERT);
            
            stmt.setString(1, coche.getPlaca());//al orden de la instruccion de atributo
            stmt.setString(2, coche.getModelo());
            stmt.setString(3, coche.getMarca());
            stmt.setDouble(4, coche.getPrecioDias());
            
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }

    public static int update(Coche coche) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_UPDATE);

            stmt.setString(1, coche.getPlaca());//al orden de la instruccion de atributo
            stmt.setString(2, coche.getModelo());
            stmt.setString(3, coche.getMarca());
            stmt.setDouble(4, coche.getPrecioDias());
            stmt.setInt(5, coche.getIdCoche());

            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
    
    public static int delete(Coche coche) {
        Connection con = null;
        PreparedStatement stmt = null;
        int registros = 0;

        con = Conexion.getConexion();
        try {
            stmt = con.prepareStatement(SQL_DELETE);
            stmt.setInt(1, coche.getIdCoche());
            registros = stmt.executeUpdate();

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            Conexion.close(con);
            Conexion.close(stmt);
        }

        return registros;
    }
}
