     <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
              <!--Aquí va el content, hereeeeee
              
              -->
                        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Responsive Table</h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="#">Ubold</a>
                        </li>
                        <li>
                            <a href="#">Tables</a>
                        </li>
                        <li class="active">
                            Responsive Table
                        </li>
                    </ol>
                </div>
            </div>



            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">

                        
                        <div class="table-rep-plugin">
                            <div class="table-wrapper"><div class="btn-toolbar"><div class="btn-group focus-btn-group"><button class="btn btn-default btn-primary"><span class="glyphicon glyphicon-screenshot"></span> Focus</button></div><div class="btn-group dropdown-btn-group pull-right"><button class="btn btn-default">Display all</button><button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Display <span class="caret"></span></button><ul class="dropdown-menu"><li class="checkbox-row"><input type="checkbox" name="toggle-tech-companies-1-col-1" id="toggle-tech-companies-1-col-1" value="tech-companies-1-col-1"> <label for="toggle-tech-companies-1-col-1">Last Trade</label></li><li class="checkbox-row"><input type="checkbox" name="toggle-tech-companies-1-col-2" id="toggle-tech-companies-1-col-2" value="tech-companies-1-col-2"> <label for="toggle-tech-companies-1-col-2">Trade Time</label></li><li class="checkbox-row"><input type="checkbox" name="toggle-tech-companies-1-col-3" id="toggle-tech-companies-1-col-3" value="tech-companies-1-col-3"> <label for="toggle-tech-companies-1-col-3">Change</label></li><li class="checkbox-row"><input type="checkbox" name="toggle-tech-companies-1-col-4" id="toggle-tech-companies-1-col-4" value="tech-companies-1-col-4"> <label for="toggle-tech-companies-1-col-4">Prev Close</label></li><li class="checkbox-row"><input type="checkbox" name="toggle-tech-companies-1-col-5" id="toggle-tech-companies-1-col-5" value="tech-companies-1-col-5"> <label for="toggle-tech-companies-1-col-5">Open</label></li><li class="checkbox-row"><input type="checkbox" name="toggle-tech-companies-1-col-6" id="toggle-tech-companies-1-col-6" value="tech-companies-1-col-6"> <label for="toggle-tech-companies-1-col-6">Bid</label></li><li class="checkbox-row"><input type="checkbox" name="toggle-tech-companies-1-col-7" id="toggle-tech-companies-1-col-7" value="tech-companies-1-col-7"> <label for="toggle-tech-companies-1-col-7">Ask</label></li><li class="checkbox-row"><input type="checkbox" name="toggle-tech-companies-1-col-8" id="toggle-tech-companies-1-col-8" value="tech-companies-1-col-8"> <label for="toggle-tech-companies-1-col-8">1y Target Est</label></li></ul></div></div><div class="table-responsive" data-pattern="priority-columns">
                                <div class="sticky-table-header fixed-solution" style="height: 39px; visibility: hidden; width: auto; top: -1px;"><table id="tech-companies-1-clone" class="table  table-striped">
                                    <thead>
                                        <tr>
                                            <th id="tech-companies-1-col-0-clone">idPaciente</th>
                                            <th data-priority="1" id="tech-companies-1-col-1-clone">Nombre</th>
                                            <th data-priority="3" id="tech-companies-1-col-2-clone">Apellido Paterno</th>
                                            <th data-priority="1" id="tech-companies-1-col-3-clone">Apellido MAterno</th>
                                            <th data-priority="3" id="tech-companies-1-col-4-clone">Sexo</th>
                                            <th data-priority="3" id="tech-companies-1-col-5-clone">Edad</th>
                                            <th data-priority="3" id="tech-companies-1-col-4-clone">Usuario</th>
                                            <th data-priority="3" id="tech-companies-1-col-5-clone">Password</th>
                                        </tr>
                                    </thead>
                                    <?php foreach ($personas1 as $key => $persona){?>
                               
        
                                        <tr>
                                            <th colspan="1" data-columns="tech-companies-1-col-0"><span class="co-name"><?=$persona->idPaciente?></span></th>
                                            <td data-priority="1" colspan="1" data-columns="tech-companies-1-col-1"><?=$persona->Nombre?></td>
                                            <td data-priority="3" colspan="1" data-columns="tech-companies-1-col-2"><?=$persona->Apaterno?></td>
                                            <td data-priority="1" colspan="1" data-columns="tech-companies-1-col-3"><?=$persona->Amaterno?></td>
                                            <td data-priority="3" colspan="1" data-columns="tech-companies-1-col-4"><?=$persona->Edad?></td>
                                            <td data-priority="3" colspan="1" data-columns="tech-companies-1-col-5"><?=$persona->Usuario?></td>
                                            <td data-priority="3" colspan="1" data-columns="tech-companies-1-col-5"><?=$persona->Password?></td>
                                            <td class="text-tigth"> 
                                            <a href="<?=base_url('index.php/Eduardo/editar')?>/<?=$persona->idPaciente?>">Editar</a>
                                            <a href="<?=base_url('index.php/Eduardo/eliminar')?>/<?=$persona->idPaciente?>">Eliminar</a>                                 
                                            </td> 
                                        </tr>

                                    <?php } ?>

                                </table></div><table id="tech-companies-1" class="table  table-striped focus-on">
                                    <thead>
                                   
                                        <tr>
                                            <th id="tech-companies-1-col-0">idPaciente</th>
                                            <th data-priority="1" id="tech-companies-1-col-1">Nombre</th>
                                            <th data-priority="3" id="tech-companies-1-col-2">Apellido Paterno</th>
                                            <th data-priority="1" id="tech-companies-1-col-3">Apellido Materno</th>
                                            <th data-priority="3" id="tech-companies-1-col-4">Edad</th>
                                            <th data-priority="3" id="tech-companies-1-col-4-clone">Usuario</th>
                                            <th data-priority="3" id="tech-companies-1-col-5-clone">Password</th>
                                    </thead>
                                    <?php foreach ($personas1 as $key => $persona){?>
                                        <tr class="unfocused">
                                            <th colspan="1" data-columns="tech-companies-1-col-0"><span class="co-name"><?=$persona->idPaciente?></span></th>
                                            <td data-priority="1" colspan="1" data-columns="tech-companies-1-col-1"><?=$persona->Nombre?></td>
                                            <td data-priority="3" colspan="1" data-columns="tech-companies-1-col-2"><?=$persona->Apaterno?></td>
                                            <td data-priority="1" colspan="1" data-columns="tech-companies-1-col-3"><?=$persona->Amaterno?></td>
                                            <td data-priority="3" colspan="1" data-columns="tech-companies-1-col-4"><?=$persona->Edad?></td>
                                            <td data-priority="3" colspan="1" data-columns="tech-companies-1-col-5"><?=$persona->Usuario?></td>
                                            <td data-priority="3" colspan="1" data-columns="tech-companies-1-col-5"><?=$persona->Password?></td>
                                            <td class="text-tigth"> 
                                            <a href="<?=base_url('index.php/Eduardo/editar')?>/<?=$persona->idPaciente?>">Editar</a>
                                            <a href="<?=base_url('index.php/Eduardo/eliminar')?>/<?=$persona->idPaciente?>">Eliminar</a>
                                            </td> 
                                
                                        </tr>
                                    <?php } ?>

                                 
                                    
                            
                                </table>
                            </div></div>

                        </div>

                    </div>
                </div>
            </div>
            <!-- end row -->

        
             <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
