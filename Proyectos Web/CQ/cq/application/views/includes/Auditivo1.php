

<!--background-->


	<!--Aquí empieza menuu-->



<!-- https://www.pinterest.com/pin/75576099974715878/ -->
<div class="nav__bar">
    <a href="#" class="nav__trigger">
    <p><img src="https://i.ibb.co/QPcM26S/libros.png" alt="menu" border="0"></p>
    </a>        
</div>

<main class="main">


<h1 style="color:black" > Unidad 1 </h1>
    <div class="bg-agile">
	<div class="book-appointment">



	
    <section class="hero">
        <div class="hero__content">
            <h1 class="hero__heading">CONCEPTOS GENERALES DE LA INFORMÁTICA</h1>

        </div>
    </section>
	<br>	<br>	<br>		<br>		

	  <div id="header">
			<ul class="navList">
				<li><a href="#ANTECEDENTES">ANTECEDENTES </a></li>
				<li><a href="#Elementos">ELEMENTOS DE UN EQUIPO DE CÓMPUTO</a>
					<ul>
						<li><a href="#INTRODUCCION">INTRODUCCIÓN</a></li>
						<li><a href="#HARDWARE">HARDWARE</a></li>
						<li><a href="#SOFTWARE">SOFTWARE </a></li>
						<li><a href="#REDES">REDES</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<br>		<br>		<br>		<br>	<br>	<br>	<br>		<br>		

	<h3 class="heading" id="ANTECEDENTES">1.1 ANTECEDENTES </h3>

    
    <section class="content">
        <article class="article">

				<iframe width="560" height="315" src="https://www.youtube.com/embed/K03aRsKR4YI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            <time class="article__time">12 de Abril de 2021</time>
            
        </article>
        
        
    </section>

		<h3 class="heading" id="Elementos">1.2 ELEMENTOS DE UN EQUIPO DE CÓMPUTO</h3><br>

		<h4 class="heading" id="INTRODUCCION">		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.2.1 INTRODUCCIÓN</h4>

    
		<section class="content">
				<article class="article">

				<iframe width="560" height="315" src="https://www.youtube.com/embed/wDzxyxXPPPs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

						<time class="article__time">12 de Abril de 2021</time>
						
				</article>
				
				
		</section>

		<h4 class="heading" id="HARDWARE"> 		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp	1.2.2 HARDWARE</h4>
    
		<section class="content">
				<article class="article">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/-jyNtg6JGRM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					
							<time class="article__time">12 de Abril de 2021</time>
						
				</article>
				
				
		</section>

		<h4 class="heading" id="SOFTWARE">		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp	1.2.3 SOFTWARE</h4>
    
			<section class="content">
					<article class="article">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/d8FBM-OvZEI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				
						<time class="article__time">12 de Abril de 2021</time>
							
					</article>
					
					
			</section>

	

		<h4 class="heading" id="REDES">		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp	1.2.4 REDES </h4>
    
		<section class="content">
				<article class="article">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/z7Q_NRGyKt4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<time class="article__time">12 de Abril de 2021</time>
						
				</article>
				
				
		</section>
	

		
   
	<div class="copy w3ls">
		       <p>&copy; 2021. All Rights Reserved </p>
	        </div>

			<br>
</main>

<nav class="navv">
    <ul class="nav__list">
				<li class="botonR"><a href="<?= base_url('index.php/AMEG/Auditivo')?>">Introducción</a></li>
        <li class="botonR"><a href="<?= base_url('index.php/AMEG/Auditivo1')?>">Unidad 1</a></li>
        <li class="botonR"><a href="<?= base_url('index.php/AMEG/Auditivo2')?>">Unidad 2</a></li>
        <li class="botonR"><a href="<?= base_url('index.php/AMEG/Auditivo3')?>">Unidad 3</a></li>
        <li class="botonR"><a href="<?= base_url('index.php/AMEG/Auditivo4')?>">Unidad 4</a></li>
        <li class="botonR"><a href="<?= base_url('index.php/AMEG/Auditivo5')?>">Unidad 5</a></li>
        <li class="botonR"><a href="<?= base_url('index.php/AMEG/Auditivo6')?>">Unidad 6 </a></li>
    </ul>
</nav>



	<!--Aquí termina menu-->
				


	<!--
			<script>
			$(document).on('ready',function(){ 

			$('#boton').click(function(){
				var url = "http://127.0.0.1/cq/index.php/AMEG/guardar";
				$.ajax({                        
				type: "POST",                 
				url: url,                     
				data: $("#test").serialize(), 
				success: function(data)             
				{
					alert("vientos");

					console.log("Hecho");
					$('#resultado').html(data);               
				}
			});
			});

			});

			</script>-->
			<script>
let navigation = {
  // Variables
  $navTrigger: document.querySelector('.nav__trigger'),
  $nav: document.querySelector('.navv'),
  $navItems: document.querySelectorAll('.nav__item a'),
  $main: document.querySelector('.main'),
  transitionEnd: 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
  isOpeningNav: false,

  init() {
    let self = this;

    // Reset overflow and height on load
    self.$main.style.overflow = 'auto';
    self.$main.style.height = 'auto';


    // Handle scroll events
    window.addEventListener('scroll', e => {
      if (window.scrollY == 0 && self.isOpeningNav) {
        self.isOpeningNav = false;

        // Add a small delay
        setTimeout(function () {
          self.openNavigation();
        }, 150);
      }
    });

    // Handle .nav__trigger click event
    self.$navTrigger.addEventListener('click', e => {
      e.preventDefault();

      if (!self.$navTrigger.classList.contains('is-active')) {
        if (window.scrollY !== 0) {
          // Scroll to top
          window.scroll({ top: 0, left: 0, behavior: 'smooth' });

          // Enable opening nav
          self.isOpeningNav = true;
        } else {
          self.openNavigation();
        }
      } else {
        self.closeNavigation();
      }
    });

    // Handle .nav__item click events
    self.$navItems.forEach(navLink => {
      navLink.addEventListener('click', function (e) {
        e.preventDefault();

        // Remove is-active from all .nav__items
        self.$navItems.forEach(el => {
          el.classList.remove('is-active');
        });

        // Ad is-active to clicked .nav__item
        this.classList.add('is-active');

        // Transition the page
        self.transitionPage();
      });
    });
  },

  openNavigation() {
    let self = this;

    // .nav--trigger active
    self.$navTrigger.classList.add('is-active');

    // body froze
    document.body.classList.add('is-froze');

    // Remove old inline styles
    if (self.$main.style.removeProperty) {
      self.$main.style.removeProperty('overflow');
      self.$main.style.removeProperty('height');
    } else {
      self.$main.style.removeAttribute('overflow');
      self.$main.style.removeAttribute('height');
    }

    // .main active
    self.$main.classList.add('is-active');
  },

  closeNavigation() {
    let self = this;

    // .nav--trigger inactive
    self.$navTrigger.classList.remove('is-active');

    // .main inactive
    self.$main.classList.remove('is-active');
    self.$main.addEventListener('transitionend', e => {
      if (e.propertyName == 'transform' && !self.$navTrigger.classList.contains('is-active')) {
        // Reset overflow and height
        self.$main.style.overflow = 'auto';
        self.$main.style.height = 'auto';

        // body unfroze
        document.body.classList.remove('is-froze');
      }
    });

    // no-csstransitions fallback
    if (document.documentElement.classList.contains('no-csstransitions')) {
      // .main inactive
      self.$main.classList.remove('is-active');

      // body unfroze
      document.body.classList.remove('is-froze');
    }
  },
/*
  transitionPage() {
    let self = this;

    // .main transitioning
    self.$main.classList.add('is-transition-out');
    self.$main.addEventListener('transitionend', e => {
      if (e.propertyName == 'clip-path') {
        if (self.$main.classList.contains('is-transition-in')) {
          self.$main.classList.remove('is-transition-in');
          self.$main.classList.remove('is-transition-out');
          self.closeNavigation();
        }

        if (self.$main.classList.contains('is-transition-out')) {
          self.$main.classList.remove('is-transition-out');

          // Add new content to .main

          setTimeout(function () {
            self.$main.classList.add('is-transition-in');
          }, 500);
        }
      }
    });
  } */
};


navigation.init();
</script>


			


