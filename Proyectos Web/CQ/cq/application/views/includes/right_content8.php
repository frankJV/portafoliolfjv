<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                  <br><br><br>

						
						
                        <div class="row">
							
							
							<div class="col-lg-10">
								<div class="card-box">
									<h4 class="m-t-0 header-title"><b>Mis datos</b></h4>
									<p class="text-muted font-13 m-b-30">
	                                    Todos sus datos son confidenciales
	                                </p>
	                                
									<form class="form-horizontal" action="editarDr" method="post" role="form" data-parsley-validate="" novalidate="">

		
									<div class="form-group1">
											<label for="inputEmail3" class="col-sm-2 control-label">Nombre</label>
											<div class="col-sm-2">
												<input type="text" required="" parsley-type="text" class="form-control" name="Nombre" id="Nombre" placeholder="Brenda" data-parsley-id="17" value="<?=$persona->Nombre?>">
												<?=form_error('Nombre')?>
											</div>
										</div>

                                        <div class="form-group1">
											<label for="inputEmail3" class="col-sm-2 control-label">Apellido Paterno</label>
											<div class="col-sm-2">
												<input type="text" required="" parsley-type="text" class="form-control" name="Apaterno" id="Apaterno" placeholder="Ortega" data-parsley-id="17" value="<?=$persona->Apaterno?>">
												<?=form_error('Apaterno')?>
											</div>
										</div>
                                        
                                        <div class="form-group1">
											<label for="inputEmail3" class="col-sm-2 control-label">Apellido Materno</label>
											<div class="col-sm-2">
												<input type="text" required="" parsley-type="text" class="form-control" name="Amaterno" id="Amaterno" placeholder="Mendez" data-parsley-id="17" value="<?=$persona->Amaterno?>" >
												<?=form_error('Amaterno')?>

											</div>
										</div>
                                       <br><br><br><br><br>     

                                        <div class="form-group3">
											<label for="inputEmail3" class="col-sm-4 control-label">Fecha de Nacimiento</label>
											<div class="col-sm-7">
												<input type="date" required="" parsley-type="date" class="form-control" name="Edad" id="Edad" data-parsley-id="17" value="<?=$persona->FN?>">
												<?=form_error('Edad')?>
											</div>
										</div>
										<br><br><br>  
										
										<div class="form-group10">
											<label for="inputEmail5" class="col-sm-3 control-label">Cédula profesional</label>
											<div class="col-sm-3">
												<input type="text" disabled="disabled" required="" parsley-type="text" class="form-control" name="Cedula" id="Cedula" placeholder="2088793" data-parsley-id="17" value="<?=$persona->Cedula?>">
												<?=form_error('Cedula')?>
											</div>
										</div>
									
										<div class="form-group10">
											<label for="inputEmail5" class="col-sm-2 control-label">Especialidad</label>
											<div class="col-sm-3">
												<input type="text" disabled="disabled" required="" parsley-type="text" class="form-control" name="Especialidad" id="Especialidad" placeholder="Dermatología" data-parsley-id="17" value="<?=$persona->Especialidad?>">
												<?=form_error('Especialidad')?>
											</div>
										</div>
										<br><br><br>  

										<div class="form-group11">
											<label for="inputEmail11" class="col-sm-3 control-label">Costo por consulta</label>
											<div class="col-sm-3">
												<input type="double"  required="" parsley-type="double" class="form-control" name="CostoConsulta" id="costoConsulta" placeholder="99.99" data-parsley-id="17" value="<?=$persona->CostoConsulta?>">
												<?=form_error('CostoConsulta')?>
											</div>
										</div>
										

										<div class="form-group11">
											<label for="inputEmail11" class="col-sm-1 control-label">Telefono</label>
											<div class="col-sm-3">
												<input type="text" required="" parsley-type="text" class="form-control" name="Telefono" id="Telefono" placeholder="2461234567" data-parsley-id="17" value="<?=$persona->Telefono?>">
												<?=form_error('Telefono')?>
											</div>
										</div>
                                        <br><br><br> 
										<div class="caja">
											<select name="Estado" id="Estado">
											<option disabled="disabled" value="SEL">--Seleccionar Estado--</option>
												<!--PONER ALARTA -->
												<option value="Estado de México">Estado de México</option>
												<option value="Aguascalientes">Aguascalientes</option>
												<option value="Baja California">Baja California</option>
												<option value="Baja California Sur">Baja California Sur</option>
												<option value="Campeche">Campeche</option>
												<option value="Chiapas">Chiapas</option>
												<option value="Chihuahua">Chihuahua</option>
												<option value="Coahuila">Coahuila</option>
												<option value="Colima">Colima</option>
												<option value="Durango">Durango</option>
												<option value="Guanajuato">Guanajuato</option>
												<option value="Guerrero">Guerrero</option>
												<option value="Hidalgo">Hidalgo</option>
												<option value="Jalisco">Jalisco</option>
												<option value="Ciudad de México">Ciudad de México</option>
												<option value="Michoacán">Michoacán</option>
												<option value="Morelos">Morelos</option>
												<option value="Nayarit">Nayarit</option>
												<option value="Nuevo León">Nuevo León</option>
												<option value="Oaxaca">Oaxaca</option>
												<option value="Puebla">Puebla</option>
												<option value="Querétaro">Querétaro</option>
												<option value="Quintana Ro">Quintana Roo</option>
												<option value="San Luis Potosí">San Luis Potosí</option>
												<option value="Sinaloa">Sinaloa</option>
												<option value="Sonora">Sonora</option>
												<option value="Tabasco">Tabasco</option>
												<option value="Tamaulipas">Tamaulipas</option>
												<option value="Tlaxcala">Tlaxcala</option>
												<option value="Veracruz">Veracruz</option>
												<option value="Yucatán">Yucatán</option>
												<option value="Zacatecas">Zacatecas</option>

											</select>
										</div>

										<div class="caja">
											<select name="Sexo" id="Sexo">
											<option disabled="disabled" value="M">--Seleccionar sexo--</option>
												<option  value="M">M</option>
												<option value="F">F</option>
											</select>
										</div>
										
										<div class="form-group4">
											<label for="inputEmail3" class="col-sm-4 control-label">Experiencia</label>
											<div  class="col-sm-7">
												<input type="email" required="" parsley-type="text" class="form-control" name="Experiencia" id="Experiencia"  placeholder="Egresado de la UNAM" data-parsley-id="17" value="<?=$persona->Experiencia?>">
												<?=form_error('Experiencia')?>
											</div>
										</div>
                                        <br><br><br>  
                                        <div class="form-group4">
											<label for="inputEmail3" class="col-sm-4 control-label">Correo</label>
											<div class="col-sm-7">
												<input type="email" required="" parsley-type="text" class="form-control" name="Correo" id="Correo"  placeholder="correo@email.com" data-parsley-id="17" value="<?=$persona->Correo?>">
												<?=form_error('Usuario')?>
											</div>
										</div>
                                        <br><br><br>  

										<div class="form-group5">
											<label for="hori-pass1" class="col-sm-3 control-label">Password*</label>
											<div class="col-sm-2">
												<input id="hori-pass1" type="password" placeholder="Password" required="" name="Password" class="form-control" id="password" data-parsley-id="19" value="<?=$persona->Password?>">
												<?=form_error('Password')?>
											</div>
										</div>


										<div class="form-group5">
											<label for="hori-pass2" class="col-sm-3 control-label">Confirm Password *</label>
											<div class="col-sm-2">
												<input data-parsley-equalto="#hori-pass1" type="password" required="" name="password1" placeholder="Password" class="form-control" id="password1" data-parsley-id="21" value="<?=$persona->Password?>">
											</div>
										</div>
										
                                        <br><br><br>  
		
										<div class="form-group">
											<div class="col-sm-offset-5 col-sm-8">
												<button type="submit" class="btn btn-primary waves-effect waves-light">
													Modificar
												</button>
											
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
										
                        
                        

    
    
     

            		</div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer">
                    2015 © Ubold.
                </footer>

            </div>