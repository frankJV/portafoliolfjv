<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class ChatModel extends CI_Model {
     
     
    public function getMensajesCanalOnTime($notnick){
        $this->db->where('TIMESTAMPDIFF(SECOND,creado_en,NOW()) <= 2',null,false);
        $this->db->where('m_chat_usuario !=',$notnick);
        $query = $this->db->get('chat_mensaje');
        return $query->result_array();
    }
     
     
    public function addMensaje($usuario,$mensaje){
        $data = array(
  				'm_chat_usuario' => $usuario,
                'chat_mensaje' => $mensaje,
                        );
        $this->db->set('creado_en', 'NOW()', FALSE);
        return $this->db->insert('chat_mensaje', $data);
    }

     
}
