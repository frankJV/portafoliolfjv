<html>
<head>
<title>Simple chat by danidomen</title>
<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script>
			$(document).ready(function (){
			var styloFila2 = "background-color:#F0F0F0";
				var styloFila1 = "background-color:#FFF";
				var nMensaje = 1;
				var idMensajeReportado = 0;
				var enviado = false;
				
				$('#chat_mensaje').bind('keyup', function(e){
					if(e.keyCode==13){
						if(!enviado){
							enviado = true;
							addVentanaChatContenido();
						}
						
					}
				});
				
				function addVentanaChatContenido(){  
					$.ajax({
						type: "POST",
						data: "usuario=<?php echo $usuario;?>&mensaje="+$('#chat_mensaje').val(),
						url: '<?php echo base_url();?>chat/add_mensaje_canal',
						dataType: "json",
						cache: false,
						success: function(data){
							$("#chat_mensaje").val(""); 
							stiloFila = styloFila2;
							if(nMensaje%2!=0){
								stiloFila = styloFila1;
							}
							$("#TableChatContent").append(''+
													'<tr style="'+stiloFila+'">'+
														'<td width="85%"><strong>@'+data.usuario+': </strong><i>'+data.mensaje+'</i></td>'+
														'<td style="font-size:10px"><i title="'+data.fecha.substr(0,10)+'">'+data.fecha.substr(11,8)+'</i></td>'+
													'</tr>');
							$("#chat_content").scrollTop(5000000);
							nMensaje++;
							enviado=false;
						}
						
					});
				}
				
				function recargarVentanaChat(){
					setInterval(function(){
						$.ajax({
				type: "POST",
							dataType: "json",
							url: '<?php echo base_url();?>chat/mensajes_canal',
							cache: false,
				data: "usuario=<?php echo $usuario;?>",
							success: function(data){
								$.each(data,function(index,value) {
									stiloFila = styloFila2;
									$('#trMensaje_'+data[index].id_chat_mensaje).remove();        
				if(nMensaje%2!=0){
				stiloFila = styloFila1;
				}
				$("#TableChatContent").append(''+
					'<tr id="trMensaje_'+data[index].id_chat_mensaje+'" style="'+stiloFila+'">'+
					'<td width="85%"><strong>@'+data[index].m_chat_usuario+': </strong><i>'+data[index].chat_mensaje+'</i></td>'+
					'<td style="font-size:10px"><i title="'+data[index].creado_en.substr(0,10)+'">'+data[index].creado_en.substr(11,8)+'</i></td>'+
					'</tr>');
				nMensaje++;
										
								});
								
			
			
							}
							
						});
						$("#chat_content").scrollTop(5000000);
					},2000);
				}
			
				recargarVentanaChat();
			
			});
</script>
</head>
<body>
 <table>
  <tbody>
<tr>
   <td><div id="chat_content" style="height: 300px; overflow-x: hidden; overflow-y: auto; width: 100%;">
<table id="TableChatContent" style="width: 100%px;" cellspacing="0" cellpadding="0" border="0">
     </table>
</div>
</td>
  </tr>
<tr>
   <td><input id="chat_mensaje" maxlength="140" placeholder="Escribe aquí para chatear..." style="width: 460px;" type="text" value="">
   </td>
  </tr>
</tbody></table>
</body>
</html>
